﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SiteShoutMVC.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;


namespace SiteShoutMVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        
        public async Task<JsonResult> GetUpperAsync(string str)
        {

            using (var httpClient = new HttpClient())
            {
                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

                // Pass the handler to httpclient(from you are calling api)
                HttpClient client = new HttpClient(clientHandler);

                using (var response = await client.GetAsync("https://localhost:44324/Upper?str=" + str))
                {

                    Console.WriteLine("response : " + response);

                    string apiResponse = await response.Content.ReadAsStringAsync();
                    Console.WriteLine("apiresponse : " + apiResponse);

              
                    Console.WriteLine("txt : " + str);
                    try
                    {
                        return Json(apiResponse);
                    }
                    catch
                    {
                        Response.StatusCode = 500;
                        return Json("Une erreur s'est produite");
                    }

                }
            }
        }
    }
}
